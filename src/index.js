/**
 * Copyright (c) 2016-present, Callstack Sp z o.o.
 * All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  View,
  Text,
  StyleSheet,
  Switch,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import { PublisherView, SubscriberView, Session } from 'react-native-opentok';
import { OPENTOK_API_KEY, SESSION_ID, PUBLISHER_TOKEN, SUBSCRIBER_TOKEN } from './variables';

const {height, width} = Dimensions.get('window');


export default function Subscriber() {
  return (
    <SubscriberView
      apiKey={OPENTOK_API_KEY}
      sessionId={OPENTOK_SESSION_ID}
      token={OPENTOK_SUBSCRIBER_TOKEN}
      style={{ width: 300, height: 200 }}
    />
  );
}

class Basic extends Component {

  componentWillMount() {
    Session.connect(OPENTOK_API_KEY, SESSION_ID, PUBLISHER_TOKEN || SUBSCRIBER_TOKEN);
    Session.onMessageRecieved((e) => console.log(e));
  }

  state = {
    isPublisher: true,
  }


  render() {
    const { isPublisher } = this.state;
    return (
      <View style={styles.container}>
        <View style={{ position: 'absolute', top: 0, left: 0, width: width, height: height }}>
            <SubscriberView
              apiKey={OPENTOK_API_KEY}
              sessionId={SESSION_ID}
              token={SUBSCRIBER_TOKEN}
              style={{ width: width, height: height }}
           />
        </View>
        <View style={{ position: 'absolute', bottom: 10, left: 10, zIndex: 10 }}>
            <PublisherView
              apiKey={OPENTOK_API_KEY}
              sessionId={SESSION_ID}
              token={PUBLISHER_TOKEN}
              style={{ width: 100, height: 100 }}
           />
        </View>
        <View style={{ position: 'absolute', bottom: 10, right: 10, zIndex: 10 }}>
           <View style={{ width: width - 150, flexDirection: 'row' }}>
             <View style={{ flexDirection: 'column', flex: 0.3, justifyContent: 'center', alignItems
             : 'center' }}>
                <TouchableOpacity style={{ width: 30, height: 30, borderRadius: 15, backgroundColor: 'green' }} />
             </View>
             <View style={{ flexDirection: 'column', flex: 0.3, justifyContent: 'center', alignItems
             : 'center' }}>
                <TouchableOpacity style={{ width: 30, height: 30, borderRadius: 15, backgroundColor: 'red' }} />
             </View>
             <View style={{ flexDirection: 'column', flex: 0.3, justifyContent: 'center', alignItems
             : 'center' }}>
                <TouchableOpacity style={{ width: 30, height: 30, borderRadius: 15, backgroundColor: 'blue' }} />
             </View>             
           </View>
        </View>
        <View style={{ position: 'absolute', top: 10, right: 10 }}>
          <TouchableOpacity style={{ paddingVertical: 7, paddingHorizontal: 14, backgroundColor: 'orange', borderRadius: 7 }}>
           <Text style={{ fontSize: 12, color: '#ffffff' }}>Switch to Tele-therapy</Text>
          </TouchableOpacity>
        </View>
      </View>   
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});

AppRegistry.registerComponent('tokbox', () => Basic);
